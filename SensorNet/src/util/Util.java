package util;

import java.io.*;
import java.net.*;
import java.util.Calendar;
import java.util.Collection;

/**
 * Created by richard on 6/10/16.
 */
public class Util {

  public static final boolean DEBUG = false;
  public static final boolean SLEEP = false;

  private static final String PUBLIC_IP = "http://checkip.amazonaws.com/";

  /**
   * Checks if port is available to bind.
   *
   * @param port Port to check against
   * @return true if port is available, else false
   */
  public static boolean isAvailableLocalPort(int port) {
    boolean available = true;
    try {
      new ServerSocket(port).close();
    } catch (Exception se) {
      available = false;
    }
    return available;
  }

  /**
   * Calculate this midgnight in milliseconds.
   *
   * @return long midnight in milliseconds.
   */
  public static long midnight() {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTimeInMillis();
  }

  /**
   * Returns a string of type "HH:MM:SS" in milliseconds from midgnight.
   *
   * @param time String of type "HH:MM:SS".
   * @return time in milliseconds from midnight.
   */
  public static long convertTime(String time) {
    /* assuming time is a not null string "HH:MM:SS" */
    String[] tmp = time.split(":");
    int hours = Integer.parseInt(tmp[0]);
    int minutes = Integer.parseInt(tmp[1]);
    int seconds = Integer.parseInt(tmp[2]);

    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, hours);
    c.set(Calendar.MINUTE, minutes);
    c.set(Calendar.SECOND, seconds);
    c.set(Calendar.MILLISECOND, 0);

    long now = c.getTimeInMillis();

    return now - midnight();
  }

  /**
   * Returns average of collection of doubles.
   *
   * @param coll A collection of Numbers
   * @return average value of collection.
   */
  public static double average(Collection<? extends Number> coll) {
    double sum = 0;
    for (Number x : coll) {
      sum += x.doubleValue();
    }
    return coll.size() > 0? sum / coll.size() : 0;
  }

  /**
   * Gives difference between millis and now in human readable format.
   *
   * @param millis Millisecond to calculate offset.
   */
  public static String getPeriod(long millis) {
    long period = System.currentTimeMillis() - millis;

    int hours = (int) ((period / 1000) / 3600);
    int minutes = (int) (((period / 1000) / 60) % 60);
    int seconds = (int) ((period / 1000) % 60);

    return String.format("%d hours, %d mins, %d seconds", hours, minutes, seconds);
  }

  /**
   * Sends a notification to client of address and port to notify node connection.
   *
   * @param address Address of client.
   * @param port Port of client.
   * @param message Message to notify to client.
   * @throws IOException
   */
  public static void sendNotification(String address, int port, String message, boolean inOut) throws IOException {
    /* if inOut == true -> notification login, else notification logout */
    String method = "notification-logout";
    if (inOut) {
      method = "notification-login";
    }

    Socket socket = new Socket(address, port);
    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    out.write(String.format("%s\r\n\r\n%s", method, message));
    out.flush();
    out.close();
    socket.close();
  }

  /**
   * Checks if a string is of format "HH:MM:SS".
   *
   * @param time String representing time of day.
   * @return true if format is respected, else false.
   */
  public static boolean areTimeDigits(String time) {
    if (time.length() != 8 || !time.contains(":")) {
      return false;
    }
    try {
      String[] tmp = time.split(":");
      int hours = Integer.parseInt(tmp[0]);
      int minutes = Integer.parseInt(tmp[1]);
      int seconds = Integer.parseInt(tmp[2]);

      if (!(hours < 24 && minutes < 60 && seconds < 60)) {
        return false;
      }
    } catch (NumberFormatException nfe) {
      return false;
    }
    return true;
  }

  /**
   * Checks if t1 time is less than t2 time.
   *
   * @param t1 String "HH:MM:SS".
   * @param t2 String "HH:MM:SS".
   * @return true if t1 < t2, else false.
   */
  public static boolean validTime(String t1, String t2) {
    /* assuming strings are already correct */
    try {
      String[] tmp1 = t1.split(":");
      int time1 = Integer.parseInt(tmp1[0] + tmp1[1] + tmp1[2]);
      String[] tmp2 = t2.split(":");
      int time2 = Integer.parseInt(tmp2[0] + tmp2[1] + tmp2[2]);

      /* check if t1 < t2 */
      if (!(time1 < time2)) {
        return false;
      }
    } catch (NumberFormatException nfe) {
      return false;
    }
    return true;
  }

  /**
   * Given "hello", returns "Hello".
   *
   * @param s A string.
   * @return String with first letter capitalized.
   */
  public static String capitalize(String s) {
    String tmp = s;
    char[] c_tmp = s.toCharArray();
    if (c_tmp.length > 0) {
      c_tmp[0] = Character.toUpperCase(c_tmp[0]);
    }
    return new String(c_tmp);
  }

  /**
   * Returns public IP address.
   *
   * @return String containing IP address.
   */
  public static String getIP() {
    BufferedReader br = null;
    try {
      URL url = new URL(PUBLIC_IP);
      br = new BufferedReader(new InputStreamReader(url.openStream()));
      return br.readLine();
    } catch (MalformedURLException mue) {
      /* impossible, url is in code */
    } catch (IOException ioe) {
      return null;
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException ioe) {}
      }
    }
    return null;
  }

  /**
   * Send a message to address, using sockets, with requested method and content
   * and optionally wait for confirmation.
   *
   * @param address Address of receiver.
   * @param method Method to request.
   * @param content Content of message.
   * @param waitForOk If an ack is required
   * @throws IOException
   */
  public static void sendMessage(InetSocketAddress address, String method, String content,
                                 boolean waitForOk) throws IOException {
    Socket s = new Socket(address.getHostString(), address.getPort());
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
    BufferedReader br = null;
    if (waitForOk) {
      br = new BufferedReader(new InputStreamReader(s.getInputStream()));
    }
    bw.write(String.format("%s\r\n\r\n%s\r\n", method, content));
    bw.flush();
    s.shutdownOutput();
    if (waitForOk) {
      /* if an ok is not received, throw an exception */
      if (!br.readLine().equals("ok")) {
        throw new IOException();
      }
      br.close();
    }
    bw.close();
    s.close();
  }

  /**
   * Send and ok message to address.
   *
   * @param bw Socket BufferedReader.
   * @throws IOException
   */
  public static void sendOK(BufferedWriter bw) throws IOException {
    bw.write("ok\r\n");
    bw.flush();
  }

}
