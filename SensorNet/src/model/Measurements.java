package model;

import simulators.Measurement;
import util.Util;

import java.util.*;

/**
 * Created by richard on 6/10/16.
 */
public class Measurements {

  private static volatile Measurements instance;
  private Map<String, List<Measurement>> store;

  private Measurements() {
    store = new HashMap<>();
    store.put("accelerometer", new ArrayList<Measurement>());
    store.put("light", new ArrayList<Measurement>());
    store.put("temperature", new ArrayList<Measurement>());
  }

  /**
   * Correct way to ask for an instance.
   *
   * @return instance The only instance present.
   */
  public static synchronized Measurements getInstance() {
    if (instance == null) {
      instance = new Measurements();
    }
    return instance;
  }

  /**
   * Add Measurements from list, diving them by type.
   *
   * @param list List of Measurements.
   */
  public void addAll(List<Measurement> list) {
    if (Util.SLEEP) {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException ie) {}
    }

    for (Measurement x : list) {
      synchronized (store) {
        /* get right list by type and add measurement */
        List<Measurement> tmp = store.get(x.getType());
        tmp.add(x);
      }
    }
  }

  /**
   * Gets the most recent measurement for Sensor by name.
   *
   * @param name Name of sensor.
   * @return Most recent measurement or null.
   */
  public Measurement mostRecent(String name) {
    if (Util.SLEEP) {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException ie) {}
    }

    Measurement measurement = null;

    /* retrieve type of sensor */
    String type = Nodes.getInstance().getType(name);

    if (type != null) {
      /* search for last measurement of node into list */
      synchronized (store) {
        List<Measurement> tmp = store.get(type);
        /* iter list in reverse order */
        for (ListIterator<Measurement> it = tmp.listIterator(tmp.size()); it.hasPrevious(); ) {
          Measurement x = it.previous();
          if (name.equals(x.getId())) {
            measurement = x;
            break;
          }
        }
      }
    }
    return measurement;
  }

  /**
   * Returns a summary (avg, min, max) of measurements for a sensor.
   *
   * @param name Name of sensor.
   * @param t1 Start time.
   * @param t2 End time.
   * @return Summary of measurements corresponding to criteria.
   */
  public Summary summaryNode(String name, String t1, String t2) {
    if (Util.SLEEP) {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException ie) {}
    }

    Summary summary = null;
    List<Double> values = new ArrayList<>();
    long startMillis = Util.convertTime(t1);
    long endMillis = Util.convertTime(t2);
    double avg = 0, min = 0, max = 0;

    /* retrieve type of sensor */
    String type = Nodes.getInstance().getType(name);

    if (type != null) {
      /* search for measurements of node into correct list */
      synchronized (store) {
        for (Measurement x : store.get(type)) {
          if (name.equals(x.getId()) && x.getTimestamp() > startMillis && x.getTimestamp() < endMillis) {
            values.add(Double.parseDouble(x.getValue()));
          }
        }
      }
    }

    /* check if values contains something */
    if (values.size() > 0) {
      summary = new Summary(name, t1, t2, Util.average(values), Collections.min(values), Collections.max(values));
    }

    return summary;
  }

  /**
   * Returns a summary (avg, min, max) of measurements for a type of sensor.
   *
   * @param type Type of sensor.
   * @param t1 Start time.
   * @param t2 End time.
   * @return Summary of measurements corresponding to criteria.
   */
  public Summary summaryType(String type, String t1, String t2) {
    if (Util.SLEEP) {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException ie) {}
    }

    Summary summary = null;
    List<Double> values = new ArrayList<>();
    long startMillis = Util.convertTime(t1);
    long endMillis = Util.convertTime(t2);
    double avg = 0, min = 0, max = 0;

    /* search for measurements of node into correct list */
    if (store.containsKey(type)) {
      synchronized (store) {
        for (Measurement x : store.get(type)) {
          if (x.getTimestamp() > startMillis && x.getTimestamp() < endMillis) {
            values.add(Double.parseDouble(x.getValue()));
          }
        }
      }
    }

    /* check if values contains something */
    if (values.size() > 0) {
      summary = new Summary(type, t1, t2, Util.average(values), Collections.min(values), Collections.max(values));
    }

    return summary;
  }

}
