package model;

import com.google.gson.Gson;

import java.util.*;

public class NodeList {

  private List<Node> nodes;

  public NodeList() {}
  
  public void setNodes(List<Node> n) {
    nodes = n;
  }
  
  public List<Node> getNodes() {
    return nodes;
  }

  public static NodeList fromJson(String json) {
    return new Gson().fromJson(json, NodeList.class);
  }

}