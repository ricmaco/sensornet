package model;

import util.Util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Better not to use a simple List to store Nodes, because we want to warrant atomicity, for example between checking
 * if a node is present and therefore adding one.
 *
 * Created by richard on 6/2/16.
 */
@XmlRootElement(name="root")
@XmlAccessorType(XmlAccessType.FIELD)
public class Nodes {

  private Set<Node> nodes;
  private static volatile Nodes instance;

  /* Constructor should be private, but because of JAXB serialization must be public
   * DO NOT USE IT or bad things could happen */
  public Nodes() {
    nodes = new LinkedHashSet<>();
  }

  /**
   * Correct way to ask for an instance.
   *
   * @return instance The only instance present.
   */
  public static synchronized Nodes getInstance() {
    if (instance == null) {
      instance = new Nodes();
    }
    return instance;
  }

  /**
   * Check if a {@link Node} is already present (by name).
   *
   * @param name name of {@link Node} to check against.
   * @return true if {@link Node} is present.
   */
  public boolean checkIfPresent(String name) {
    synchronized (nodes) {
      Set<Node> tmp = new HashSet<>(nodes);
      return tmp.contains(new Node(name, null, null, 0, 0L));
    }
  }

  /**
   * Retrieve type of sensor from name.
   *
   * @param name Name of sensor.
   * @return String type of sensor.
   */
  public String getType(String name) {
    String type = null;
    synchronized (nodes) {
      for (Node x : nodes) {
        if (name.equals(x.getName())) {
          type = x.getType();
          break;
        }
      }
    }
    return type;
  }

  /**
   * Adds a {@link Node} if it is not already present.
   *
   * @param node {@link Node} to be added.
   * @return true if {@link Node} has been added.
   */
  public boolean addNode(Node node) {
    boolean result = false;
    synchronized (nodes) {
      if (Util.SLEEP) {
        try {
          Thread.sleep(5000);
        } catch (InterruptedException ie) {}
      }

      if (nodes.add(node)) {
        result = true;
      }
    }
    return result;
  }

  /**
   * Deletes a {@link Node} from list.
   *
   * @param name Name of {@link Node} to be deleted.
   * @return true if deletion has been done (false if {@link Node} does not exist in users).
   */
  public boolean deleteNode(String name) {
    boolean result = false;
    synchronized (nodes) {
      if (nodes.remove(new Node(name, null, null, 0, 0L))) {
        result = true;
      }
    }
    return result;
  }

  /**
   * Returns a view over the current list of Users.
   *
   * @return List containing Users.
   */
  public List<Node> getNodes() {
    return new ArrayList<>(nodes);
  }

}
