package model;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by richard on 6/10/16.
 */
@XmlRootElement
public class Summary {

  @XmlElementRef(name="type_or_name")
  private String typeOrName;
  private String t1;
  private String t2;
  private double avg;
  private double min;
  private double max;

  public Summary() {}

  public Summary(String typeOrName, String t1, String t2, double avg, double min, double max) {
    this.typeOrName = typeOrName;
    this.t1 = t1;
    this.t2 = t2;
    this.avg = avg;
    this.min = min;
    this.max = max;
  }

  public String getTypeOrName() {
    return typeOrName;
  }

  public void setTypeOrName(String typeOrName) {
    this.typeOrName = typeOrName;
  }

  public String getT1() {
    return t1;
  }

  public void setT1(String t1) {
    this.t1 = t1;
  }

  public String getT2() {
    return t2;
  }

  public void setT2(String t2) {
    this.t2 = t2;
  }

  public double getAvg() {
    return avg;
  }

  public void setAvg(double avg) {
    this.avg = avg;
  }

  public double getMin() {
    return min;
  }

  public void setMin(double min) {
    this.min = min;
  }

  public double getMax() {
    return max;
  }

  public void setMax(double max) {
    this.max = max;
  }

}
