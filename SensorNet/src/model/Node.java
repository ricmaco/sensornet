package model;

import com.google.gson.Gson;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

/**
 * Created by richard on 6/2/16.
 */
@XmlRootElement
public class Node {

  private String name;
  private String type;
  private String address;
  private int port;
  private long timestamp;

  public Node() {}

  public Node(String name, String type, String address, int port, long timestamp) {
    this.name = name;
    this.type = type;
    this.address = address;
    this.port = port;
    this.timestamp = timestamp;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public String getAddress() {
    return address;
  }

  public int getPort() {
    return port;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * Using default implementation of Java7.
   *
   * @return hashcode of class members.
   */
  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  /**
   * Object are compared only by name.
   *
   * @param obj Object to be compared
   * @return true if name.equals(obj.name)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Node) {
      Node n = (Node) obj;
      return name.equals(n.getName());
    }
    return false;
  }

  /**
   * Returns the instance a dictionary JSON string.
   *
   * @return JSON string
   */
  @Override
  public String toString() {
    return new Gson().toJson(this);
  }

}
