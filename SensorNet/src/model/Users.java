package model;

import util.Util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Better not to use a simple List to store Users, because we want to warrant atomicity, for example between checking
 * if a user is present and therefore adding one.
 *
 * Created by richard on 6/1/16.
 */
@XmlRootElement(name="root")
@XmlAccessorType(XmlAccessType.FIELD)
public class Users {

  private Set<User> users;
  private static volatile Users instance;

  /* Constructor should be private, but because of JAXB serialization must be public
   * DO NOT USE IT or bad things could happen */
  public Users() {
    users = new HashSet<>();
  }

  /**
   * Correct way to ask for an instance.
   *
   * @return instance The only instance present.
   */
  public static synchronized Users getInstance() {
    if (instance == null) {
      instance = new Users();
    }
    return instance;
  }

  /**
   * Check if a {@link User} is already present (by name).
   *
   * @param name name of {@link User} to check against.
   * @return true if user is present.
   */
  public boolean checkIfPresent(String name) {
    synchronized (users) {
      Set<User> tmp = new HashSet<>(users);
      return tmp.contains(new User(name, null, 0));
    }
  }

  /**
   * Adds a {@link User} if it is not already present.
   *
   * @param user {@link User} to be added.
   * @return true if {@link User} has been added.
   */
  public boolean addUser(User user) {
    boolean result = false;
    synchronized (users) {
      if (Util.SLEEP) {
        try {
          Thread.sleep(5000);
        } catch (InterruptedException ie) {}
      }

      if (users.add(user)) {
        result = true;
      }
    }
    return result;
  }

  /**
   * Deletes a {@link User} from list.
   *
   * @param name Name of {@link User} to be deleted.
   * @return true if deletion has been done (false if {@link User} does not exist in users).
   */
  public boolean deleteUser(String name) {
    boolean result = false;
    synchronized (users) {
      if (users.remove(new User(name, null, 0))) {
        result = true;
      }
    }
    return result;
  }

  /**
   * Returns a view over the current list of Users.
   *
   * @return List containing Users.
   */
  public List<User> getUsers() {
    synchronized (users) {
      return new ArrayList<>(users);
    }
  }
}
