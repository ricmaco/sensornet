package model;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import simulators.Measurement;
import util.Util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by richard on 6/9/16.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Token {

  @XmlTransient
  public static transient final int CAPACITY = 15;

  private List<Measurement> measurements;

  public Token() {
    measurements = new LinkedList<>();
  }

  /**
   * Retrieve remaining space to reach capacity.
   */
  private int getRemainingSpace() {
    return CAPACITY - measurements.size();
  }

  public boolean isFull() {
    return measurements.size() == CAPACITY;
  }

  /**
   * Add all elements possible to the token, always leaving token with a max
   * of CAPACITY elements.
   *
   * @param list List of Measurements.
   */
  public void addAll(List<Measurement> list) {
    /* check if list is not empty */
    if (list.size() > 0) {
      /* insert last up to 15 measurements, depending on remaining space.
       * other previous measurements are lost. */
      int firstIndex = Math.max(list.size() - getRemainingSpace(), 0);
      if (Util.DEBUG) {
        System.out.printf("Token remaining: %d\n", getRemainingSpace());
      }
      int lastIndex = list.size();
      List<Measurement> tmp = list.subList(firstIndex, lastIndex);
      measurements.addAll(tmp);
    }
  }

  public void clear() {
    measurements.clear();
  }

  public List<Measurement> getMeasurements() {
    return new ArrayList<>(measurements);
  }

  /**
   * Gives a String representing this in JSON format.
   *
   * @return JSON String.
   */
  public String serialize() {
    return new Gson().toJson(this);
  }

  /**
   * Builds a Token from a JSON String.
   *
   * @param json String representing Token in JSON.
   * @return Token object from JSON.
   */
  public static Token deserialize(String json) {
    try {
      return new Gson().fromJson(json, Token.class);
    } catch (JsonSyntaxException jse) {
      System.err.println("Received Token is invalid, request not fulfilled.");
      System.exit(248);
    }
    return null;
  }
}
