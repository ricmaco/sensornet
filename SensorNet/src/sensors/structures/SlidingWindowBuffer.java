package sensors.structures;

import simulators.Buffer;
import simulators.Measurement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by richard on 5/6/16.
 */
public class SlidingWindowBuffer implements Buffer<Measurement> {

  private static final int SIZE = 15;

  private List<Measurement> queue;
  private List<Measurement> temp;
  private Long referenceTS;

  /**
   * Inits an empty queue of measurements.
   */
  public SlidingWindowBuffer() {
    queue = new LinkedList<>();
    temp = new ArrayList<>();
    referenceTS = -1L;
  }

  /**
   * Return the average measurement of the temporary list, and clear the list.
   */
  private Measurement averageAndClear() {
    double v_sum = 0;
    long t_sum = 0L;
    int size = temp.size();

    /* if temp has something */
    if (size > 0) {
      /* save last measurement for retrieving id, type and timestamp */
      Measurement x = null;
      for (Iterator<Measurement> it = temp.iterator(); it.hasNext(); ) {
        x = it.next();
        /* we need average value... */
        v_sum += Double.parseDouble(x.getValue());
        /* ...and average timestamp */
        t_sum += x.getTimestamp();
      }
      temp.clear();
      return new Measurement(
        x.getId(), /* id of last record */
        x.getType(), /* type of last record */
        (v_sum / size) + "", /* mean value */
        t_sum / size /* mean timestamp */
      );
    }
    return null;
  }

  /**
   * Insert a {@link Measurement} into the queue, in a circular way.
   *
   * @param measurement the {@link Measurement} to be inserted.
   */
  @Override
  public void add(Measurement measurement) {
    /* if we're inside a window, add measurement to temp list */
    if (measurement.getTimestamp() < referenceTS) {
      temp.add(measurement);
      return;
    }

    /* else obtain the average of previous values and insert into queue (and clear
     * temporary list) if there are values into temp */
    Measurement avg = averageAndClear();
    if (avg != null) {
      synchronized (this) {
        if (queue.size() == SIZE) {
          queue.remove(0);
        }
        queue.add(avg);
      }
    }

    /* update reference timestamp and insert new value into temporary list */
    referenceTS = measurement.getTimestamp() + 1000L;
    temp.add(measurement);
  }

  /**
   * Return a list of at most SIZE elements and clears local buffer.
   *
   * @return List<Measurement> of at most SIZE elements.
   */
  @Override
  public List<Measurement> readAllAndClean() {
    List<Measurement> tmp = null;
    synchronized (this) {
      tmp = new ArrayList<>(queue);
      queue.clear();
    }
    return tmp;
  }
}
