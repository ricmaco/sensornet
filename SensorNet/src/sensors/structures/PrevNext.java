package sensors.structures;

import model.Node;
import util.Util;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by richard on 6/9/16.
 */
public class PrevNext {

  private InetSocketAddress address = null;
  private InetSocketAddress prev = null;
  private InetSocketAddress next = null;

  private final Object prevLock = new Object();
  private final Object nextLock = new Object();

  public PrevNext() {}

  public boolean isUnique() {
    synchronized (prevLock) {
      synchronized (nextLock) {
        return prev.equals(address) && next.equals(address);
      }
    }
  }

  public InetSocketAddress getAddress() {
    return address;
  }

  public void setAddress(String address, int port) {
    this.address = InetSocketAddress.createUnresolved(address, port);
    /* set prev to my address */
    setPrev(this.address);
    setNext(this.address);
  }

  public InetSocketAddress getPrev() {
    synchronized (prevLock) {
      return prev;
    }
  }

  public InetSocketAddress getNext() {
    synchronized (nextLock) {
      return next;
    }
  }

  public void setPrev(InetSocketAddress address) {
    synchronized (prevLock) {
      prev = address;
      if (Util.DEBUG) {
        System.out.printf("Prev set to %s:%d\n", address.getHostString(), address.getPort());
      }
    }
  }

  public void setPrev(String address, int port) {
    synchronized (prevLock) {
      this.prev = InetSocketAddress.createUnresolved(address, port);
      if (Util.DEBUG) {
        System.out.printf("Prev set to %s:%d\n", address, port);
      }
    }
  }

  public void setNext(InetSocketAddress address) {
    synchronized (nextLock) {
      next = address;
      if (Util.DEBUG) {
        System.out.printf("Next set to %s:%d\n", address.getHostString(), address.getPort());
      }
    }
  }

  public void setNext(String address, int port) {
    synchronized (nextLock) {
      this.next = InetSocketAddress.createUnresolved(address, port);
      if (Util.DEBUG) {
        System.out.printf("Next set to %s:%d\n", address, port);
      }
    }
  }

  /**
   * Sets the list of nodes and calculate other values (unique, prev, next),
   * based on list.
   *
   * @param nodes A list of nodes.
   */
  public void setNodes(List<Node> nodes) {
    /* check if node is the only one present */
    if (nodes.size() > 1) {
      Node tmp = null;
      /* set prev (and first call to set*, setting first to false */
      tmp = nodes.get(nodes.size() - 2);
      setPrev(tmp.getAddress(), tmp.getPort());

      /* and set next */
      tmp = nodes.get(0);
      setNext(tmp.getAddress(), tmp.getPort());
    }
  }

}
