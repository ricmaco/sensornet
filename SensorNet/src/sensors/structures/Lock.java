package sensors.structures;

import java.util.Objects;

/**
 * Created by richard on 6/8/16.
 */
public class Lock {

  private boolean lock;

  public Lock() {
    lock = false;
  }

  /**
   * Get lock value (synchronized).
   */
  public synchronized boolean status() {
    return lock;
  }

  /**
   * Set lock value (synchronized).
   */
  public synchronized void setLocked() {
    this.lock = true;
  }

  /**
   * Using default implementation of Java7.
   *
   * @return hashcode of class members.
   */
  @Override
  public int hashCode() {
    return Objects.hash(lock);
  }

  /**
   * Object are compared only by value.
   *
   * @param obj Object to be compared.
   * @return true if name.equals(obj.lock).
   */
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Lock) {
      Lock l = (Lock) obj;
      return lock == l.status();
    }
    return false;
  }

  /**
   * Returns the lock value.
   *
   * @return lock Lock value.
   */
  @Override
  public String toString() {
    return lock + "";
  }

}
