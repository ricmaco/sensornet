package sensors.structures;

import simulators.Buffer;
import simulators.Measurement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by richard on 5/6/16.
 */
public class SimpleBuffer implements Buffer<Measurement> {

  private static final int SIZE = 15;

  private List<Measurement> queue;

  /**
   * Inits an empty queue of measurements.
   */
  public SimpleBuffer() {
    queue = new LinkedList<>();
  }

  /**
   * Returns the size of the queue.
   *
   * @return
   */
  public synchronized int size() {
    return queue.size();
  }

  /**
   * Insert a {@link Measurement} into the queue, in a circular way.
   *
   * @param measurement the {@link Measurement} to be inserted.
   */
  @Override
  public synchronized void add(Measurement measurement) {
    /* if queue is full, delete the first element and add the new one */
    if (queue.size() == SIZE) {
      queue.remove(0);
    }
    queue.add(measurement);
  }

  /**
   * Return a list of at most SIZE elements and clears local buffer.
   *
   * @return List<Measurement> of at most SIZE elements.
   */
  @Override
  public List<Measurement> readAllAndClean() {
    List<Measurement> tmp = null;

    synchronized (this) {
      tmp = new ArrayList<>(queue);
      queue.clear();
    }
    return tmp;
  }

}
