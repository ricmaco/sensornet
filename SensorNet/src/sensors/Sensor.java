package sensors;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import model.IncompleteNode;
import model.NodeList;
import sensors.workers.StarterWorker;
import simulators.*;
import sensors.structures.Lock;
import sensors.structures.PrevNext;
import sensors.structures.SimpleBuffer;
import sensors.structures.SlidingWindowBuffer;
import sensors.workers.CommunicationWorker;
import sensors.workers.InputWorker;
import util.Util;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.*;
import java.util.*;

/**
 * Created by richard on 5/6/16.
 */
public class Sensor {

  private String name, sAddress;
  private char type;
  private static final Map<Character, String> typeMap;
  static {
    Map<Character, String> tmp = new HashMap<>();
    tmp.put('t', "temperature");
    tmp.put('l', "light");
    tmp.put('a', "accelerometer");
    typeMap = Collections.unmodifiableMap(tmp);
  }
  private String address;
  private int port;

  private Simulator simulator = null;
  private Buffer<Measurement> buffer = null;

  private Client client = null;
  private PrevNext prevNext = null;

  private Thread simulatorThread = null;
  private Thread inputThread = null;
  private Thread communicationThread = null;
  private Thread starterThread = null;

  public static volatile Lock isStopping = new Lock();
  private final Object ready = new Object();

  /**
   * List of error codes:
   *   - 255: wrong command line parameters
   *   - 254: type not valid
   *   - 253: port not valid
   *   - 252: gateway address not valid
   *   - 251: name not valid
   *   - 250: cannot start server
   *   - 249: cannot communicate with socket
   *   - 248: invalid token
   *
   * @param argv Command line args.
   */
  private Sensor(String[] argv) {
    if (argv.length < 4) {
      System.err.printf("Usage: %s <id> <type> <port> <gateway:port>\n", Sensor.class.getCanonicalName());
      System.exit(255);
    }

    /* read name of sensor */
    name = argv[0];

    /* read and evaluate type */
    type = argv[1].charAt(0);
    if (type != 't' && type != 'l' && type != 'a') {
      System.err.println("<type> field must be {t,l,a}.");
      System.exit(254);
    }

    /* read and evaluate port */
    port = 0;
    try {
      port = Integer.parseInt(argv[2]);
      if (!Util.isAvailableLocalPort(port)) {
        System.err.printf("Port '%d' is already taken, choose another.\n", port);
        System.exit(253);
      }
    } catch (NumberFormatException nfe) {
      System.err.println("<port> argument must be a number.");
      System.exit(253);
    }

    /* read and evaluate sAddress address */
    sAddress = String.format("http://%s", argv[3]);
    try {
      URI uri = new URI(sAddress);
      if (uri.getHost() == null || uri.getPort() == -1) {
        throw new URISyntaxException(uri.toString(), "URI does not contain hostname or port");
      }
      sAddress = uri.toString();
    } catch (URISyntaxException use) {
      System.err.println("<sAddress> argument must be a valid address:port tuple.");
      System.exit(252);
    }

    System.out.println("Starting Nodo 1.0.0...\n");

    /* init jersey client */
    /* enable POJO mapping */
    ClientConfig clientConfig = new DefaultClientConfig();
    clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
    client = Client.create(clientConfig);
    /* set timeout to 10 seconds */
    client.setConnectTimeout(10000);

    /* init prevnext */
    prevNext = new PrevNext();
  }

  /**
   * Creates a Sensor statically
   *
   * @param argv Command line arguments
   * @return New Sensor
   */
  public static Sensor createSensor(String[] argv) {
    Sensor sensor = new Sensor(argv);
    sensor.login();
    sensor.startSimulator();

    return sensor;
  }

  /**
   * Starts a simulator, based on type variable.
   */
  private void startSimulator() {
    switch (type) {
      case 't':
        simulator = new TemperatureSimulator(name, buffer = new SimpleBuffer());
        break;
      case 'l':
        simulator = new LightSimulator(name, buffer = new SimpleBuffer());
        break;
      case 'a':
        simulator = new AccelerometerSimulator(name, buffer = new SlidingWindowBuffer());
        break;
    }
    simulatorThread = new Thread(simulator);
    simulatorThread.start();
  }

  /**
   * Connects sensor to gateway.
   */
  private void login() {
    /* get corresponding type string */
    String typeString = typeMap.get(type);

    /* get address based on context, localhost or public ip */
    if (sAddress.contains("localhost") || sAddress.contains("127.0.0.1")) {
      address = "localhost";
    } else {
      address = Util.getIP();
    }

    IncompleteNode node = new IncompleteNode(name, typeString, address, port);

    try {
      /* make the request */
      ClientResponse response = client
        .resource(sAddress)
        .path("/node/login")
        .entity(node, "application/json")
        .post(ClientResponse.class);

      /* exit if name is not valid else save nodes list */
      if (response.getStatus() == Response.Status.OK.getStatusCode()) {
        /* save list of nodes into specific class holder */
        prevNext.setAddress(address, port);
        prevNext.setNodes(NodeList.fromJson(response.getEntity(String.class)).getNodes());
      } else {
        System.err.println(response.getEntity(String.class));
        System.exit(251);
      }
    } catch (ClientHandlerException che) {
      /* if there is an exception, provided address in in code, gateway is surely down */
      System.err.println("Gateway is down, please retry.");
      System.exit(251);
    }
  }

  /**
   * Starts a public server to listen to connections.
   */
  public void start() {
    try {
      /* init the server waiting for other nodes connections
       * requisite: all servers must be multithreaded */
      ServerSocket sSocket = new ServerSocket(port);

      /* init terminal controller thread with stopper class */
      inputThread = new Thread(new InputWorker());
      inputThread.start();

      /* if i'm the only one try to send a new empty token to myself */
      if (prevNext.isUnique()) {
        starterThread = new Thread(new StarterWorker(ready, prevNext));
        starterThread.start();

        /* wait until thread is ready to init token listener */
        synchronized (ready) {
          try {
            ready.wait();
          } catch (InterruptedException e) {}
        }
      } else {
        /* login to prev and next */
        try {
          /* send message to prev, setting its next to me */
          Util.sendMessage(prevNext.getPrev(), "login-prev", String.format("%s:%d", address, port), true);

          /* send message to next, setting its prev to me */
          Util.sendMessage(prevNext.getNext(), "login-next", String.format("%s:%d", address, port), true);
        } catch (IOException ioe) {
          System.err.println("Cannot communicate with socket.");
          System.exit(249);
        }
      }

      if (Util.SLEEP) {
        try {
          Thread.sleep(5000);
        } catch (InterruptedException ie) {}
      }

      while (true) { /* stopped by a ServerSocket.close() */
        /* start to accept connections */
        Socket socket = sSocket.accept();

        communicationThread = new Thread(new CommunicationWorker(sSocket, socket, buffer, prevNext, name,
          sAddress, client));
        communicationThread.start();
      }
    } catch (SocketException se) {
      /* accept call intercepted -> normal behaviour */
    } catch (IOException ioe) {
      System.err.printf("Cannot start server on port %d.\n", port);
      System.exit(251);
    }

    /* wait for all the threads to stop */
    try {
      inputThread.join();
      if (communicationThread != null) {
        communicationThread.join();
      }
      if (starterThread != null) {
        starterThread.join();
      }
    } catch (InterruptedException ie) {}

    /* stop server and simulators with silk glove */
    simulator.stopMeGently();
  }

  /**
   * Disconnects sensor from gateway.
   */
  public void stop() {
    int status = -1;
    String entity = null;

    /* retry up to 5 times */
    for (int c=5; status!=Response.Status.OK.getStatusCode() && c>0; --c) {
      try {
        /* make the request */
        ClientResponse response = client
          .resource(sAddress)
          .path(String.format("/node/logout/%s", name))
          .delete(ClientResponse.class);

        status = response.getStatus();
        entity = response.getEntity(String.class);
      } catch (ClientHandlerException che) {
        status = -1;
      }
    }
    if (status == Response.Status.OK.getStatusCode()) {
      System.out.println(entity);
    }
  }

  public static void main(String[] args) {
    Sensor sensor = Sensor.createSensor(args);

    sensor.start(); /* warning: blocking call */
    sensor.stop();
  }

}
