package sensors.workers;

import sensors.Sensor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by richard on 5/13/16.
 */
public class InputWorker implements Runnable {

  public InputWorker() {}

  /**
   * If string "exit" is received, exits cleanly.
   */
  @Override
  public void run() {
    /* communicate with input */
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    /* if "exit" inserted stop parent with silk glove */
    try {
      String tmp;
      do {
        System.out.print("$ ");
        tmp = in.readLine();
      } while (!tmp.equalsIgnoreCase("exit"));

      System.out.println("Now closing...");
      in.close();

      /* signal intention to stop and exit */
      Sensor.isStopping.setLocked();
    } catch (IOException ioe) {}
  }

}
