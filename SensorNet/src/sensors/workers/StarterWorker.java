package sensors.workers;

import model.Token;
import sensors.structures.PrevNext;
import util.Util;

import java.io.IOException;

/**
 * Created by richard on 6/10/16.
 */
public class StarterWorker implements Runnable {

  private Object ready;
  private PrevNext prevNext;

  public StarterWorker(Object ready, PrevNext prevNext) {
    this.ready = ready;
    this.prevNext = prevNext;
  }

  @Override
  public void run() {
    /* i'm ready, send notification to main thread */
    synchronized (ready) {
      ready.notifyAll();
    }

    Token token = new Token();

    /* retry until a successful message has been sent */
    boolean error = false;
    do {
      try {
        Util.sendMessage(prevNext.getNext(), "token", token.serialize(), false);
        error = false;
      } catch (IOException ioe) {
        error = true;
      }
    } while (error);
  }
}
