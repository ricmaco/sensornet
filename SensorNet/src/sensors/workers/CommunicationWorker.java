package sensors.workers;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import model.Token;
import sensors.Sensor;
import sensors.structures.PrevNext;
import simulators.Buffer;
import simulators.Measurement;
import util.Util;

import javax.ws.rs.core.Response;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * Pseudo HTTP message type:
 *
 * login-prev\r\n
 * \r\n
 * [address:port]\r\n
 *
 * login-next\r\n
 * \r\n
 * [address:port]\r\n
 *
 * logout-prev\r\n
 * \r\n
 * [address:port]\r\n
 *
 * logout-next\r\n
 * \r\n
 * [address:port]\r\n
 *
 * token\r\n
 * \r\n
 * [json of token]\r\n
 *
 * Created by richard on 5/31/16.
 */
public class CommunicationWorker implements Runnable {

  private ServerSocket sSocket = null;
  private Socket socket = null;
  private Buffer<Measurement> buffer = null;
  private PrevNext prevNext = null;
  private String name = null;
  private String sAddress = null;
  private Client client = null;

  private BufferedReader in = null;
  private BufferedWriter out = null;


  public CommunicationWorker(ServerSocket sSocket, Socket socket, Buffer<Measurement> buffer, PrevNext prevNext,
                             String name, String sAddress, Client client) {
    this.sSocket = sSocket;
    this.socket = socket;
    this.buffer = buffer;
    this.prevNext = prevNext;
    this.name = name;
    this.sAddress = sAddress;
    this.client = client;
    try {
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    } catch (IOException ioe) {
      System.err.println("Cannot communicate with socket.");
      System.exit(249);
    }
  }

  /**
   * Sends measurements to server and clear token container.
   *
   * @param token Token to discharge and clear.
   */
  private void dischargeAndClear(Token token) {
    try {
      /* make the request */
      ClientResponse response = client
        .resource(sAddress)
        .path("/node/token")
        .queryParam("node", name)
        .entity(token, "application/json")
        .post(ClientResponse.class);

      /* exit if name is not valid else save nodes list */
      if (response.getStatus() != Response.Status.OK.getStatusCode()) {
        /* gateway error */
        System.err.println("Gateway internal error.");
        System.exit(251);
      }
    } catch (ClientHandlerException che) {
      /* if there is an exception, provided address in in code, gateway is surely down */
      System.err.println("Gateway is down, please retry.");
      System.exit(251);
    }
    token.clear();
  }

  /**
   * Handles token message (and disconnects if necessary).
   *
   * @param content Content of message.
   */
  private void token(String content) {
    if (Util.SLEEP) {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException ie) {}
    }

    /* deserialize token and add measurements */
    Token token = Token.deserialize(content);
    List<Measurement> tmp = buffer.readAllAndClean();
    token.addAll(tmp);

    if (prevNext.isUnique()) {
      if (Sensor.isStopping.status()) { /* is to be stopped */
        /* discharge data into gateway */
        dischargeAndClear(token);

        /* close ServerSocket, stopping client */
        try {
          sSocket.close();
        } catch (IOException ioe) {}
      } else { /* is not to be stopped */
        /* discharge only if full */
        if (token.isFull()) {
          dischargeAndClear(token);
        }

        /* send token to next */
        try {
          Util.sendMessage(prevNext.getNext(), "token", token.serialize(), false);
        } catch (IOException ioe) {
          System.err.println("Cannot communicate with socket.");
          System.exit(249);
        }
      }
    } else { /* not unique */
      /* discharge only if full */
      if (token.isFull()) {
        dischargeAndClear(token);
      }

      /* Sensor.isStopping value should not change between logout and stopping */
      synchronized (Sensor.isStopping) {
        /* if it is to be stopped */
        if (Sensor.isStopping.status()) {
          try {
            /* send message to my next, setting its prev to my prev */
            Util.sendMessage(prevNext.getNext(), "logout-next",
              String.format("%s:%d", prevNext.getPrev().getHostString(), prevNext.getPrev().getPort()), true);
            /* send message to my prev, setting its next to my next */
            Util.sendMessage(prevNext.getPrev(), "logout-prev",
              String.format("%s:%d", prevNext.getNext().getHostString(), prevNext.getNext().getPort()), true);
          } catch (IOException ioe) {
            System.err.println("Cannot communicate with socket.");
            System.exit(249);
          }
        }

        /* send token to next */
        try {
          Util.sendMessage(prevNext.getNext(), "token", token.serialize(), false);
        } catch (IOException ioe) {
          System.err.println("Cannot communicate with socket.");
          System.exit(249);
        }

        if (Sensor.isStopping.status()) {
          /* close ServerSocket, stopping client */
          try {
            sSocket.close();
          } catch (IOException ioe) {}
        }
      }
    }
  }

  /**
   * Handles login/logout of a node, being its next.
   *
   * @param content Content of message.
   */
  private void next(String content) {
    /* parse address and port from content */
    String[] tmp = content.split(":");
    String address = tmp[0];
    int port = Integer.parseInt(tmp[1]);
    /* set it */
    prevNext.setPrev(address, port);

    /* send ok to old prev */
    try {
      Util.sendOK(out);
    } catch (IOException e) {
      System.err.println("Cannot communicate with socket.");
      System.exit(249);
    }
  }

  /**
   * Handles login/logout of a node, being its prev.
   *
   * @param content Content of message.
   */
  private void prev(String content) {
    /* parse address and port from content */
    String[] tmp = content.split(":");
    String address = tmp[0];
    int port = Integer.parseInt(tmp[1]);
    /* set it */
    prevNext.setNext(address, port);

    /* send ok to old next */
    try {
      Util.sendOK(out);
    } catch (IOException e) {
      System.err.println("Cannot communicate with socket.");
      System.exit(249);
    }
  }

  @Override
  public void run() {
    try {
      /* read method */
      String method = in.readLine();
      /* skip \r\n padding */
      in.readLine();

      /* read until end */
      String content = "";
      String tmp = null;
      while ((tmp = in.readLine()) != null) {
        content += tmp;
      }

      switch (method) {
        /* receiving logout and i'm the next */
        case "logout-next":
          next(content);
          break;
        /* receiving logout and i'm the prev */
        case "logout-prev":
          prev(content);
          break;
        /* receiving login and i'm the next */
        case "login-next":
          next(content);
          break;
        /* receiving login and i'm the prev */
        case "login-prev":
          prev(content);
          break;
        case "token":
          token(content);
          break;
      }

      in.close();
      out.close();
      socket.close();
    } catch (IOException ioe) {
      System.err.println("Cannot communicate with socket.");
      System.exit(249);
    }
  }

}
