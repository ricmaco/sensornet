package client.workers;

import com.google.gson.Gson;
import model.IncompleteNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by richard on 6/3/16.
 */
public class NotificationWorker implements Runnable {

  private Socket socket = null;
  private BufferedReader in = null;

  public NotificationWorker(Socket socket) {
    this.socket = socket;
    try {
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    } catch (IOException ioe) {
      /* do nothing, cannot receive notification */
      System.err.println("Cannot communicate with notification socket.");
    }
  }

  @Override
  public void run() {
    try {
      String method = in.readLine();
      /* /r/n padding */
      in.readLine();
      String nodeJson = in.readLine();

      /* write notification based on method */
      switch (method) {
        case "notification-login":
          /* get an incomplete node object from nodeJson */
          IncompleteNode node = new Gson().fromJson(nodeJson, IncompleteNode.class);

          System.out.printf("\nNode '%s' of type '%s', with address '%s:%d' has connected.\nChoice: ", node.getName(),
            node.getType(), node.getAddress(), node.getPort());
          break;
        case "notification-logout":
          String nodeName = nodeJson;

          System.out.printf("\nNode '%s' has disconnected.\nChoice: ", nodeName);
          break;
        default:
          break;
      }
    } catch (IOException ioe) {
      System.err.println("Cannot communicate with notification socket.");
    }
  }

}
