package client.workers;

import client.User;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import model.Node;
import model.Nodes;
import model.Summary;
import simulators.Measurement;
import util.Util;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;

/**
 * Created by richard on 6/3/16.
 */
public class DispatchWorker implements Runnable {

  private static boolean stop = false;
  private static final Map<Integer, String> functions;
  static {
    Map<Integer, String> tmp = new TreeMap<>();
    tmp.put(1, "List of available sensors");
    tmp.put(2, "Most recent measurement for a sensor");
    tmp.put(3, "Summary (average, max, min) of measurements for a sensor from time t1 to time t2");
    tmp.put(4, "Summary (average, max, min) of measurements for a sensor type from time t1 to time t2");
    tmp.put(5, "Exit");
    functions = Collections.unmodifiableMap(tmp);
  }

  private BufferedReader in = null;
  private ServerSocket sSocket = null;
  private Client client = null;

  private String name;
  private String gateway;

  /**
   * Inits an empty input thread.
   *
   * @param name Name of User
   * @param gateway Gateway address, port included
   */
  public DispatchWorker(BufferedReader in, ServerSocket sSocket, String name, String gateway) {
    this.in = in;
    this.sSocket = sSocket;
    this.name = name;
    this.gateway = gateway;

    /* init jersey client */
    /* enable POJO mapping */
    ClientConfig clientConfig = new DefaultClientConfig();
    clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
    client = Client.create(clientConfig);
    /* set timeout to 10 seconds */
    client.setConnectTimeout(10000);
  }

  /**
   * Prints a list of choices and evaluates user input.
   *
   * @return Users choice.
   */
  private int readFunction() {
    int choice = -1;

    do {
      System.out.println();

      /* print choices */
      for (Map.Entry<Integer, String> x : functions.entrySet()) {
        System.out.printf("%d) %s\n", x.getKey(), x.getValue());
      }
      System.out.print("\nChoice: ");

      /* read choice */
      try {
        choice = Integer.parseInt(in.readLine());
      } catch (Exception e) {
        /* choice will remain -1 */
      }
      System.out.println();
    } while (choice < 1 && choice > functions.size()); /* until choice effectively done */
    return choice;
  }

  /**
   * Asks a question and checks for input correctness.
   *
   * @param question Question to ask.
   * @return String response from user.
   */
  private String ask(String question) {
    String res = null;
    do {
      try {
        /* ask question */
        System.out.printf("%s: ", question);
        /* read string from input */
        res = in.readLine();
        if (res.length() == 0) {
          System.out.println("Cannot accept an empty string, please retry.");
        }
      } catch (IOException ioe) {
        System.err.println("Cannot communicate with an input.");
      }
    } while (res == null || res.length() == 0); /* until string is read or is not empty */
    return res;
  }

  /**
   * Fetches and prints available nodes list.
   */
  private void nodesList() {
    try {
      /* make the request */
      ClientResponse response = client
        .resource(gateway)
        .path("/query/nodes")
        .queryParam("user", name)
        .accept("application/json")
        .get(ClientResponse.class);

      if (response.getStatus() == Response.Status.OK.getStatusCode()) {
        /* fetch nodes list and print it */
        List<Node> nodes = response.getEntity(Nodes.class).getNodes();
        for (Node x : nodes) {
          System.out.printf("Node: %s\n\tType: %s\n\tUptime: %s\n",
            x.getName(),
            x.getType(),
            /* calculate period of uptime */
            Util.getPeriod(x.getTimestamp())
          );
        }
      } else {
        /* if there is an error it is surely gateway fault */
        throw new ClientHandlerException();
      }
    } catch (ClientHandlerException che) {
      System.out.println("Gateway down or not working, please retry.");
    }
  }

  /**
   * Fetches and prints most recent measurement for a node.
   */
  private void recentNode() {
    try {
      /* ask which node */
      String node = ask("Node");

      /* make the request */
      ClientResponse response = client
        .resource(gateway)
        .path("/query/recent")
        .queryParam("user", name)
        .queryParam("node", node)
        .accept("application/json")
        .get(ClientResponse.class);

      if (response.getStatus() == Response.Status.OK.getStatusCode()) {
        /* fetch recent measurement and print it */
        Measurement meas = response.getEntity(Measurement.class);
        System.out.printf("Measurement from node %s:\n\tType: %s\n\tValue: %s\n\tFreshness: %s\n",
          meas.getId(),
          meas.getType(),
          meas.getValue(),
          Util.getPeriod(Util.midnight() + meas.getTimestamp())
        );
      } else {
        /* print error message */
        System.out.println(response.getEntity(String.class));
      }
    } catch (ClientHandlerException che) {
      System.out.println("Gateway down or not working, please retry.");
    }
  }

  /**
   * Fetches and prints summary (avg, min, max) of measurements for a node
   * or a type.
   *
   * @param choice "node" or "type".
   */
  private void summaryNodeOrType(String choice) {
    try {
      /* ask which node or type, t1 and t2 */
      String nodeOrType = ask(Util.capitalize(choice));
      String t1 = ask("T1 (format HH:MM:SS)");
      String t2 = ask("T2 (format HH:MM:SS)");

      /* check if t1 and t2 are correct, else return */
      if (!(Util.areTimeDigits(t1) && Util.areTimeDigits(t2) && Util.validTime(t1, t2))) {
        System.out.println("Parameters T1 and T2 invalid, please retry.");
        return;
      }

      /* make the request */
      ClientResponse response = client
        .resource(gateway)
        .path("/query/summary")
        .queryParam("user", name)
        .queryParam(choice, nodeOrType)
        .queryParam("t1", t1)
        .queryParam("t2", t2)
        .accept("application/json")
        .get(ClientResponse.class);

      if (response.getStatus() == Response.Status.OK.getStatusCode()) {
        /* fetch recent measurement and print it */
        Summary summary = response.getEntity(Summary.class);
        System.out.printf(
          "Summary for %s %s:\n" +
            "\tFrom time: %s\n" +
            "\tTo time: %s\n" +
            "\tAverage: %f\n" +
            "\tMinimum: %f\n" +
            "\tMaximum: %f\n",
          choice,
          summary.getTypeOrName(),
          summary.getT1(),
          summary.getT2(),
          summary.getAvg(),
          summary.getMin(),
          summary.getMax()
        );
      } else {
        /* print error message */
        System.out.println(response.getEntity(String.class));
      }
    } catch (ClientHandlerException che) {
      System.out.println("Gateway down or not working, please retry.");
    }
  }

  @Override
  public void run() {
    while (!stop) {
      switch (readFunction()) {
        case 1: /* list of sensors */
          nodesList();
          break;
        case 2: /* recent measurement for node */
          recentNode();
          break;
        case 3:
          summaryNodeOrType("node");
          break;
        case 4:
          summaryNodeOrType("type");
          break;
        case 5: /* exit */
          try {
            stop = true;
            in.close();
            sSocket.close();
          } catch (IOException ioe) {}
          User.stopMeGently();
          break;
        default:
          break;
      }
    }
  }

}
