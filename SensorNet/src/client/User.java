package client;

import client.workers.DispatchWorker;
import client.workers.NotificationWorker;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import util.Util;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

/**
 * Created by richard on 5/30/16.
 */
public class User {

  private static final String PUBLIC_IP = "http://checkip.amazonaws.com/";

  private BufferedReader in = null;
  private Client client = null;
  private static volatile boolean stop = false;

  private String name = null;
  private String sAddress = null;
  private int sPort;
  private int port;

  private Thread dispatchThread = null;
  private Thread notificationThread = null;

  /**
   * Initialize the client.
   * Error summary:
   *   - 255: no terminal
   *   - 254: cannot start notification server
   */
  private User() {
    try {
      in = new BufferedReader(new InputStreamReader(System.in));
      /* enable POJO mapping */
      ClientConfig clientConfig = new DefaultClientConfig();
      clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
      client = Client.create(clientConfig);
      /* set timeout to 10 seconds */
      client.setConnectTimeout(10000);

      System.out.println("Starting Utente 1.0.0...\n");

      contactGateway();
      port = User.friendlyAskInt(in, "Porta notifiche");
      checkUser();
    } catch (IOException e) {
      System.err.println("Cannot communicate with an input.");
      System.exit(255);
    }
  }

  /**
   * Creates a User statically.
   *
   * @return New User.
   */
  public static User createUser() {
    return new User();
  }

  /**
   * Ask address and password of gateway.
   * Try to create a connection with gateway and see if it works.
   *
   * @throws IOException
   */
  private void contactGateway() throws IOException {
    int status = -1;
    String entity = null;
    do {
      /* ask for a correct address and port */
      sAddress = User.friendlyAskString(in, "Indirizzo gateway");
      sPort = User.friendlyAskInt(in, "Porta gateway");

      try {
        /* make the request */
        ClientResponse response = client
          .resource(String.format("http://%s:%d", sAddress, sPort))
          .path("/status")
          .accept("text/plain")
          .get(ClientResponse.class);

        status = response.getStatus();
        entity = response.getEntity(String.class);
      } catch (ClientHandlerException che) {
        status = -1;
        System.out.println("Bad gateway credentials, please insert them again.");
      }
    } while (status != Response.Status.OK.getStatusCode()); /* if 200 */
    System.out.println(entity);
  }

  /**
   * Checks that user is correct, also retrieving IP address to send to gateway.
   *
   * @throws IOException
   */
  private void checkUser() throws IOException {
    int status = -1;
    String entity = null;
    do {
      if (status > -1) {
        System.out.println(entity);
      }

      /* ask for a syntactically correct username */
      name = User.friendlyAskString(in, "Nome utente");

      /* Bad hack to get ip address based on context, if local or Internet.
       * Necessary because neither JDKHttpServer not Grizzly2 can handle HttpServletRequest.
       */
      String address = "";
      switch (sAddress) {
        /* request public ip to amazon, if down or no connection fall back to localhost */
        default:
          address = Util.getIP();
          if (address != null) {
            break;
          }
        /* if server is contacted locally, server can contact client equally locally */
        case "localhost":
        case "127.0.0.1":
          address = "localhost";
      }

      /* build user to send */
      model.User user = new model.User(name, address, port);

      try {
        /* make the request */
        ClientResponse response = client
          .resource(String.format("http://%s:%d", sAddress, sPort))
          .path("/user/login")
          .entity(user, "application/json")
          .post(ClientResponse.class);

        status = response.getStatus();
        entity = response.getEntity(String.class);
      } catch (ClientHandlerException che) {
        status = -1;
        System.out.println("User creation failed, please insert data again.");
      }
    } while (status != ClientResponse.Status.OK.getStatusCode());
    System.out.println(entity);
  }

  /**
   * Friendly ask for a String printing a question.
   *
   * @param in Where to read from.
   * @param question Question to ask.
   * @return String received in input.
   * @throws IOException
   */
  private static String friendlyAskString(BufferedReader in, String question) throws IOException {
    String tmp = "";
    do {
      System.out.printf("[?] %s: ", question);
      tmp = in.readLine();
    } while (tmp.equals("")); /* until String is not empty ask */
    return tmp;
  }

  /**
   * Friendly ask for a positive integral value printing a question.
   *
   * @param in Where to read from.
   * @param question Question to ask.
   * @return int received in input.
   * @throws IOException
   */
  private static int friendlyAskInt(BufferedReader in, String question) throws IOException {
    int tmp = -1;
    boolean notANumber = false;
    do {
      System.out.printf("[?] %s: ", question);
      try {
        tmp = Integer.parseInt(in.readLine());
      } catch (NumberFormatException nfe) {
        notANumber = true;
      }
    } while (notANumber && tmp <= 0); /* until int is not a int or is negative/zero ask */
    return tmp;
  }

  /**
   * Starts input thread and notification server, waiting for notifications.
   */
  public void start() {
    try {
      /* start notification server
       * requisite: all servers must be multithreaded */
      ServerSocket sSocket = new ServerSocket(port);

      /* start input handler thread */
      dispatchThread = new Thread(new DispatchWorker(in, sSocket, name,
        String.format("http://%s:%d", sAddress, sPort)));
      dispatchThread.start();

      while (!stop) {
        Socket socket = sSocket.accept(); /* BLOCKING CALL */

        /* start notification thread */
        notificationThread = new Thread(new NotificationWorker(socket));
        notificationThread.start();
      }
    }  catch (SocketException se) {
      /* accept call intercepted -> normal behaviour */
    } catch (IOException ioe) {
      System.err.printf("Cannot start notification server on port %d.\n", port);
      System.exit(254);
    }

    /* wait for all the threads to stop */
    try {
      dispatchThread.join();
      if (notificationThread != null) {
        notificationThread.join();
      }
    } catch (InterruptedException ie) {}
  }

  /**
   * Sends the disconnect message to gateway.
   */
  public void stop() {
    int status = -1;
    String entity = null;

    /* retry up to 5 times */
    for (int c=5; status!=Response.Status.OK.getStatusCode() && c>0; --c) {
      try {
        /* make the request */
        ClientResponse response = client
          .resource(String.format("http://%s:%d", sAddress, sPort))
          .path(String.format("/user/logout/%s", name))
          .delete(ClientResponse.class);

        status = response.getStatus();
        entity = response.getEntity(String.class);
      } catch (ClientHandlerException che) {
        status = -1;
      }
    }
    if (status == Response.Status.OK.getStatusCode()) {
      System.out.println(entity);
    }
  }

  /**
   * Stops User with a silk glove.
   */
  public static void stopMeGently() {
    stop = true;
  }

  public static void main(String[] args) {
    User client = User.createUser();

    client.start(); /* warning: blocking call */
    client.stop();
  }
}
