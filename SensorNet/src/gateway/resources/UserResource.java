package gateway.resources;

import model.User;
import model.Users;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by richard on 6/1/16.
 */
@Path("/user")
public class UserResource {

  @POST
  @Path("/login")
  @Consumes("application/json")
  public Response login(User user) {
    /* retrieve list */
    Users users = Users.getInstance();

    /* if can add user OK else ERROR */
    if (users.addUser(user)) {
      return Response.ok(String.format("User '%s' added.", user.getName())).build();
    }
    return Response
      .status(Response.Status.CONFLICT)
      .entity(String.format("User '%s' already exists.", user.getName()))
      .type("text/plain")
      .build();
  }

  @DELETE
  @Path("/logout/{user}")
  public Response logout(@PathParam("user") String user) {
    /* retrieve list */
    Users users = Users.getInstance();

    if (users.deleteUser(user)) {
      return Response.ok(String.format("User '%s' disconnected.", user)).build();
    }
    return Response
      .status(Response.Status.NOT_FOUND)
      .entity(String.format("User '%s' not found.", user))
      .type("text/plain")
      .build();
  }

  @GET
  @Path("/list")
  @Produces("application/json")
  public Response list(@QueryParam("user") String user) {
    /* retrieve list of users */
    Users users = Users.getInstance();

    if (user != null && users.checkIfPresent(user)) {
      return Response.ok(users).build();
    }
    return Response
      .status(Response.Status.UNAUTHORIZED)
      .entity("Unauthorized request. Please specify a valid user.")
      .type("text/plain")
      .build();
  }

}
