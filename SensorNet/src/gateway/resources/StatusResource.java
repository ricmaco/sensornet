package gateway.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by richard on 6/1/16.
 */
@Path("status")
public class StatusResource {

  @GET
  @Produces("text/plain")
  public Response status() {
    return Response.ok("Gateway is up and running.").type("text/plain").build();
  }

}
