package gateway.resources;

import model.*;
import util.Util;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by richard on 6/6/16.
 */
@Path("/node")
public class NodeResource {

  @POST
  @Path("/login")
  @Consumes("application/json")
  @Produces("application/json")
  public Response login(IncompleteNode node) {
    /* retrieve list */
    Nodes nodes = Nodes.getInstance();
    Users users = Users.getInstance();

    /* create a complete node from an incomplete node */
    Node n = new Node(node.getName(), node.getType(), node.getAddress(), node.getPort(), System.currentTimeMillis());

    /* if can add node OK else ERROR */
    if (nodes.addNode(n)) {
      /* send notification to clients, no ack */
      for (User x : users.getUsers()) {
        try {
          Util.sendNotification(x.getAddress(), x.getPort(), node.toString(), true);
        } catch (IOException ioe) {
          /* do nothing, because no ack */
          System.err.printf("Cannot send notification to client '%s:%d', about node '%s'.\n", x.getAddress(),
            x.getPort(), node.getName());
        }
      }
      /* return list of nodes */
      return Response.ok(nodes).build();
    }
    return Response
      .status(Response.Status.CONFLICT)
      .entity(String.format("Node '%s' already exists.", node.getName()))
      .type("text/plain")
      .build();
  }

  @DELETE
  @Path("/logout/{node}")
  public Response logout(@PathParam("node") String node) {
    /* retrieve list */
    Nodes nodes = Nodes.getInstance();
    Users users = Users.getInstance();

    if (nodes.deleteNode(node)) {
      /* send notification to clients, no ack */
      for (User x : users.getUsers()) {
        try {
          Util.sendNotification(x.getAddress(), x.getPort(), node, false);
        } catch (IOException ioe) {
          /* do nothing, because no ack */
          System.err.printf("Cannot send notification to client '%s:%d', about node '%s'.\n", x.getAddress(),
            x.getPort(), node);
        }
      }

      return Response.ok(String.format("Node '%s' disconnected.", node)).build();
    }
    return Response
      .status(Response.Status.NOT_FOUND)
      .entity(String.format("Node '%s' not found.", node))
      .type("text/plain")
      .build();
  }

  @POST
  @Path("/token")
  @Consumes("application/json")
  public Response token(@QueryParam("node") String node, Token token) {
    /* retrieve list */
    Nodes nodes = Nodes.getInstance();
    Measurements measurements = Measurements.getInstance();

    if (node != null && nodes.checkIfPresent(node)) {
      /* add measurements to structure and send ok */
      measurements.addAll(token.getMeasurements());

      return Response.ok().build();
    }
    return Response
      .status(Response.Status.UNAUTHORIZED)
      .entity("Unauthorized request. Please specify a valid node.")
      .type("text/plain")
      .build();
  }

}
