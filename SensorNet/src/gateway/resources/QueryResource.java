package gateway.resources;

import model.Measurements;
import model.Summary;
import model.Users;
import model.Nodes;
import simulators.Measurement;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by richard on 6/2/16.
 */
@Path("/query")
public class QueryResource {

  @GET
  @Path("/nodes")
  @Produces("application/json")
  public Response list(@QueryParam("user") String user) {
    /* retrieve list of users */
    Users users = Users.getInstance();
    Nodes nodes = Nodes.getInstance();

    if (user != null && users.checkIfPresent(user)) {
      return Response.ok(nodes).build();
    }
    return Response
      .status(Response.Status.UNAUTHORIZED)
      .entity("Unauthorized request. Please specify a valid user.")
      .type("text/plain")
      .build();
  }

  @GET
  @Path("/recent")
  @Produces("application/json")
  public Response recent(@QueryParam("user") String user, @QueryParam("node") String node) {
    /* retrieve list of users */
    Users users = Users.getInstance();
    Nodes nodes = Nodes.getInstance();
    Measurements measurements = Measurements.getInstance();

    if (user != null && users.checkIfPresent(user)) {
      if (node != null) {
        if (nodes.checkIfPresent(node)) {
          /* if measurement exist return it, else not found */
          Measurement measurement = measurements.mostRecent(node);
          if (measurement != null) {
            return Response.ok(measurement).build();
          }
          return Response
            .status(Response.Status.NOT_FOUND)
            .entity(String.format("No measurements from node '%s'.", node))
            .type("text/plain")
            .build();
        }
        return Response
          .status(Response.Status.NOT_FOUND)
          .entity(String.format("Node '%s' does not exist.", node))
          .type("text/plain")
          .build();
      }
      return Response
        .status(Response.Status.BAD_REQUEST)
        .entity("Parameter 'node' not specified.")
        .type("text/plain")
        .build();
    }
    return Response
      .status(Response.Status.UNAUTHORIZED)
      .entity("Unauthorized request. Please specify a valid user.")
      .type("text/plain")
      .build();
  }

  /* node parameter has the precedence over type if both are present */
  @GET
  @Path("/summary")
  @Produces("application/json")
  public Response summary(@QueryParam("user") String user, @QueryParam("node") String node,
                          @QueryParam("type") String type, @QueryParam("t1") String t1, @QueryParam("t2") String t2) {
    /* retrieve list of users */
    Users users = Users.getInstance();
    Nodes nodes = Nodes.getInstance();
    Measurements measurements = Measurements.getInstance();
    Summary summary = null;

    if (user != null && users.checkIfPresent(user)) {
      /* t1 < t2 */
      if (t1 != null && t2 != null) {
        if (node != null) {
          if (nodes.checkIfPresent(node)) {
            summary = measurements.summaryNode(node, t1, t2);
            if (summary != null) {
              return Response.ok(summary).build();
            }
            return Response
              .status(Response.Status.NOT_FOUND)
              .entity(String.format("No measurements corresponding to criteria from node '%s'.", node))
              .type("text/plain")
              .build();
          }
          return Response
            .status(Response.Status.NOT_FOUND)
            .entity(String.format("Node '%s' does not exist.", node))
            .type("text/plain")
            .build();
        }
        if (type != null) {
          summary = measurements.summaryType(type, t1, t2);
          if (summary != null) {
            return Response.ok(summary).build();
          }
          return Response
            .status(Response.Status.NOT_FOUND)
            .entity(String.format("No measurements corresponding to criteria of type '%s'.", type))
            .type("text/plain")
            .build();
        }
      }
      return Response
        .status(Response.Status.BAD_REQUEST)
        .entity("Some parameter {node,type,t1,t2} not specified or invalid.")
        .type("text/plain")
        .build();
    }
    return Response
      .status(Response.Status.UNAUTHORIZED)
      .entity("Unauthorized request. Please specify a valid user.")
      .type("text/plain")
      .build();
  }

}
