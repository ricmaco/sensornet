package gateway.workers;

import sensors.Sensor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;

/**
 * Created by richard on 5/13/16.
 */
public class InputWorker implements Runnable {

  private Object lock;

  /**
   * @param lock server locking object.
   */
  public InputWorker(Object lock) {
    this.lock = lock;
  }

  /**
   * If string "exit" is received, exits cleanly.
   */
  @Override
  public void run() {
    /* communicate with input */
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    /* if "exit" inserted stop parent with silk glove */
    try {
      String tmp;
      do {
        System.out.print("$ ");
        tmp = in.readLine();
      } while (!tmp.equalsIgnoreCase("exit"));

      System.out.println("Now closing...");

      in.close();
      synchronized (lock) {
        lock.notify();
      }
    } catch (IOException ioe) {}
  }

}
