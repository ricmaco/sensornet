package gateway;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import gateway.workers.InputWorker;
import model.Node;
import model.Nodes;
import model.User;
import model.Users;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by richard on 5/6/16.
 */
public class Server {

  private static final String HOST = "localhost";
  private volatile Object lock = new Object();

  private Thread inputWorker = null;

  private int port;
  private HttpServer server = null;

  /**
   * List of error codes:
   *   - 255: wrong command line parameters
   *   - 254: port not valid
   *   - 253: cannot start server
   *
   * @param argv Command line args.
   */
  public Server(String[] argv) {
    if (argv.length < 1) {
      System.err.printf("Usage: %s <port>\n", Server.class.getCanonicalName());
      System.exit(255);
    }

    /* read and evaluate port */
    port = 0;
    try {
      port = Integer.parseInt(argv[0]);
    } catch (NumberFormatException nfe) {
      System.err.println("<port> argument must be a number.");
      System.exit(254);
    }

    /* disable awful logging */
    Logger.getLogger("").setLevel(Level.SEVERE);

    System.out.println("Starting Gateway 1.0.0...\n");
  }

  /**
   * Start the Jersey web server.
   */
  public void startServer() {
    try {
      /* start Grizzly 2 server using UriBuilder to build address with port */
      server = HttpServerFactory
        .create(UriBuilder.fromUri(String.format("http://%s/", HOST)).port(port).build());

      /* start a thread to control user input */
      inputWorker = new Thread(new InputWorker(lock));
      inputWorker.start();

      server.start();

      /* wait a thread unblock current thread and stop server */
      synchronized (lock) {
        try {
          lock.wait();
        } catch (InterruptedException e) {}
      }
      server.stop(0);

      /* to circumvent jersey bug thread remains opened after stop */
      System.exit(0);
    } catch (Exception ioe) {
      System.err.printf("Cannot start server on port %d.\n", port);
      System.exit(253);
    }
  }

  public static void main(String[] args) throws IOException {
    Server server = new Server(args);

    server.startServer();
  }

}
