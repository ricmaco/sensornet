package gateway.exceptions;

import org.codehaus.jackson.map.exc.UnrecognizedPropertyException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by richard on 6/1/16.
 */
@Provider
public class UnrecognizedPropertyExceptionMapper implements ExceptionMapper<UnrecognizedPropertyException> {

  @Override
  public Response toResponse(UnrecognizedPropertyException exception) {
    return Response
      .status(Response.Status.BAD_REQUEST)
      .entity(String.format("Invalid request. Field \"%s\" is not recognized.",
          exception.getUnrecognizedPropertyName()))
      .type("text/plain")
      .build();
  }

}
