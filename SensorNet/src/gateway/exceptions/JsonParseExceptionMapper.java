package gateway.exceptions;

import org.codehaus.jackson.JsonParseException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by richard on 6/1/16.
 */
@Provider
public class JsonParseExceptionMapper implements ExceptionMapper<JsonParseException> {

  @Override
  public Response toResponse(JsonParseException exception) {
    return Response
      .status(Response.Status.BAD_REQUEST)
      .entity("Invalid JSON provided. The request cannot be parsed.")
      .type("text/plain")
      .build();
  }

}
