package gateway.exceptions;

import org.codehaus.jackson.map.JsonMappingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by richard on 6/1/16.
 */
@Provider
public class JsonMappingExceptionMapper implements ExceptionMapper<JsonMappingException> {

  @Override
  public Response toResponse(JsonMappingException exception) {
    return Response
      .status(Response.Status.BAD_REQUEST)
      .entity("Invalid request. Format of at least one field is not correct.")
      .type("text/plain")
      .build();
  }

}


