# SensorNet
- **Studente**: Riccardo Macoratti
- **Matricola**: 884553

#### Dependencies
- Jersey 1.12 (`core`, `server`, `multipart`)
- Jersey Client 1.12
- Gson 2.6.2

#### Struttura
Il progetto è diviso in sei package: `client`, `gateway`, `sensors`, `simulators`, `model` e `util`.
- `simulators` contiene le classi rese disponibili dal docente.
- `model` contiene le strutture dati atte a rappresentare o contenere oggetti, come `User` e `Users`, `Node` e `Nodes`, `Measurements` e altre classi di supporto.
- `util` contiene una classe con funzioni statiche di utilità, spesso utilizzate in più parti nel progetto.
- `client` contiene l'applicazione Utente e, in `workers`, i thread che gestiscono richieste e notifiche push.
- `sensors` contiene l'applicazione Nodo, che rappresenta un nodo della rete e, in `workers`, i thread che gestiscono input dell'utente, partenza del token e gestione delle richieste di login/logout e arrivo del token. `structures` contiene delle classi di utilità.
- `gateway` contiene il server gateway che interagisce con utenti e nodi; `resources` contiene le risorse REST, mentre `workers` il thread di gestione dell'input e `exceptions` le eccezioni generali per le richieste.

#### client.User
L'applicazione Utente appena aperta richiede l'indirizzo e la porta del gateway, la porta per la ricezione delle notifiche push e un nome utente. Una volta verificati questi valori presenta le varie possibilità di interrogazione del gateway (in maniera user-friendly) o la possibilità di uscire.

Le notifiche sono gestite tramite un server multithread che riceve dei messaggi del tipo:
```
notification-login\r\n
\r\n
{
    "name": "example",
    "type": "light",
    "address": "localhost",
    "port": 6666,
    "timestamp": 1465750481
}\r\n
```
```
notification-logout\r\n
\r\n
example\r\n
```

#### gateway.Server
Il gateway ha la struttura di una tipica applicazione Jersey. Le possibile risorse sono:
- `status`
- `user`
    - `login`
    - `logout`
    - `list`
- `node`
    - `login`
    - `logout`
    - `token`
- `query`
    - `nodes`
    - `recent`
    - `summary` (sia per tipo che per nodo)

La risorsa considera la richiesta valida se viene fornito come parametro GET, di volta in volta, un utente registrato o un nodo registrato, in maniera tale da garantire un'autenticazione, pur sempre rimanendo stateless.

Si avvia con:
```
java gateway.Server <port>
```

#### sensors.Sensor
Si avvia con:
```
java sensors.Sensor <id> <type> <port> <g_address:g_port>
```
`type` è un carattere tra `t` per temperature, `l` per light o `a` per accelerometer.

Inseriti dei parametri corretti, viene eseguito il login e fatto partire il server che rimane in attesa di eventuali messaggi di gestione del token, login o logout. I messaggi sono del tipo:
```
<method>\r\n
\r\n
<content>\r\n
```
`method` è una stringa tra `token`, `login-next`, `login-prev`, `logout-next` e `logout-prev` e identifica l'azione da intraprendere.
`content` è, a seconda dei casi, o una serializzazione del token, oppure l'indirizzo e la porta che devono essere sostituiti nel nodo precedente o successivo.

La criticità più evidente di questo step è stata la gestione della "modalità singola" e quindi la creazione del token. Un vincolo da rispettare è stato quello dell'**unicità del token**. Per prima cosa è necessario capire se il nodo si trovi ad essere l'unico nella rete oppure uno successivo in una rete già avviata. Successivamente, se ci si trova nel **primo caso**, si fa partire **un thread che ha il compito di inviare al nodo di cui fa parte un token vuoto**, in modo da far partire l'algoritmo token-ring, e si continua normalmente con l'altro thread già citato che gestisce i messaggi in arrivo. Se ci si trova nel **secondo caso** invece viene inviato un **messaggio di login al nodo precedente e a quello successivo** e si entra a far parte della rete.

Per effettuare il logout invece, per prima cosa si riceve dall'utente il messaggio di uscita e poi si aspetta di poter entrare in regione critica, quindi **avere il token**. Successivamente se non ci sono altri nodi presenti viene inviato il token, anche se non è pieno, al gateway, altrimenti vengono aggiunte le proprie misurazioni e lo si manda avanti al nodo successivo. Prima di rilasciare il token, vengono inviati **opportuni messaggi di logout** ai nodi vicini (nel caso in cui non essi siano presenti).

Per garantire che il cerchio non si spezzi durante l'aggiunta o l'uscita di un nodo, alla ricezione di un messaggio di login o logout ogni nodo **prima cambia il proprio puntatore al nodo precedente o successivo e solo dopo invia un messaggio di ACK**.

#### Util
La classe Util, oltre a contenere tutte le funzioni di utilità, presenta du campi booleani `DEBUG` e `SLEEP`.
- `DEBUG` se impostata a `true`, permette di vedere lo stato del token e i messaggi di login/logout scambiati tra i nodi.
- `SLEEP` se impostata a `true`, permette di inserire un ritardo di 5 secondi in alcune fasi (come le richieste o la gestione del token) per verificare la corretta gestione dell'ambiente concorrente.
















